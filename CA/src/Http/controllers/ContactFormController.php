<?php
// CA\PackageContact\src\Http\Controllers\ContactFormController.php

namespace CA\PackageContact\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use CA\PackageContact\Models\ContactForm;

class ContactFormController extends Controller {

    public function index()
    {
        return view('PackageContact::contact');
    }

    public function sendMail(Request $request)
    {
        ContactForm::create($request->all());
        return redirect(route('contact'))->with(['message' => 'Thank you, your mail has been sent successfully.']);
    }
}